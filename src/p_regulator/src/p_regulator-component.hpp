#ifndef OROCOS_P_REGULATOR_COMPONENT_HPP
#define OROCOS_P_REGULATOR_COMPONENT_HPP

#include <rtt/RTT.hpp>
#include <rtt/InputPort.hpp>
#include <rtt/OutputPort.hpp>

class P_regulator : public RTT::TaskContext{
	// Ports
	RTT::InputPort<double> ref_port;
	RTT::InputPort<double> in_port;
	RTT::OutputPort<double> out_port;

	// Properties
	double k_prop;
	std::string filename_prop;
  public:
    P_regulator(std::string const& name);
    bool configureHook();
    bool startHook();
    void updateHook();
    void stopHook();
    void cleanupHook();
};
#endif
