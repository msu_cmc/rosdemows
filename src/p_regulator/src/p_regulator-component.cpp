#include "p_regulator-component.hpp"
#include <rtt/Component.hpp>
#include <rtt/Logger.hpp>
#include <iostream>

using namespace RTT;

P_regulator::P_regulator(std::string const& name) : TaskContext(name) 
{
	addProperty("k", k_prop).
		doc("K coefficent of P-regulator").
		set(0);

	addPort("g", ref_port).
		doc("Reference signal");
	addPort("in", in_port).
		doc("Measured signal"); addPort("out", out_port).
		doc("Control signal");

	log(RTT::Info) << "P_regulator componenet constructed.";
}

bool P_regulator::configureHook()
{
	return true;
}

bool P_regulator::startHook()
{
	log(RTT::Info) << "P_regulator componenet started." << endlog();
	return true;
}

void P_regulator::updateHook() 
{
	double ref, in, out;

	ref_port.read(ref);
	in_port.read(in);

	out = k_prop * (ref - in);

	out_port.write(out);
}

void P_regulator::stopHook()
{
}

void P_regulator::cleanupHook()
{
}

/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(P_regulator)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(P_regulator)
