#!/bin/bash

if [ "$#" -lt 1 ]; then
	echo Usage: $0 FILE [T [WINTITLE]]
	echo
	echo Scripts displays FileReporter log using feedgnuplot in real time. File header must be on.
	echo FILE is log filename and WINTITLE is gnuplot windows title. T is time axis length in seconds.
	echo Also the content of log file is copied to .mat file with the same basename without header line.
	echo
	echo Modified version of feedgnuplot is used. It must support \`--headerlegend\' option.
	exit 1
fi

FIFO=$1
WINTITLE=${3:-OROGNUPLOT}
XLEN=${2:-20}

if [ ! -p $FIFO ] ; then 
	if [ -e $FIFO ]; then 
		rm -f $FIFO
	fi
	mkfifo $FIFO
fi

cat $FIFO | tee $FIFO.log | ./feedgnuplot --stream 0.1 --domain --xlen $XLEN --lines --headerlegend --title "$FIFO" --set "terminal x11 title \"$WINTITLE\""

mv -f $FIFO.log $FIFO
tail -n +2 <$FIFO >${FIFO%log}mat
